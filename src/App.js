import React, {Component} from 'react'
import './App.css'
import {Route} from 'react-router-dom'
import Homepage from './components/Homepage'

export default class App extends Component {

	render() {
		return (
		 <Route path="/" render={() => (
				<Homepage />
			)}
			/>
    )
	}
}
