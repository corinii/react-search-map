import React, {Component} from "react"
import './SideMenu.css'
import escapeRegExp from 'escape-string-regexp'
import sortBy from 'sort-by'
import {Link} from 'react-router-dom'

export default class SideMenu extends Component {
  state = {
    query: ''
  }

  updateQuery = (query) => {
    this.setState ({query: query.trim() })
  }

  render() {
  let showingVenues
  if(this.state.query) {
  const match = new RegExp(escapeRegExp(this.state.query), "i")
  showingVenues = this.props.venues.filter((venue) => match.test(venue.name))
} else {
  showingVenues = this.props.venues
}

showingVenues.sort(sortBy('name'))

    return (
      <div className="list-venues">
      {JSON.stringify(this.state)}
      <div className="list-venues-top">
      <input
      className="search-venues"
      type="text"
      placeholder="Filter Venues"
      value = {this.state.query}
      onChange={(event) => this.updateQuery(event.target.value)}
      />
      </div>
      <ol className="list">
      {showingVenues.map((venue,index) => (
          <li
          key={venue.index}
          className="list-item"
          onClick={(event) => this.props.seeListItem(venue)}>
          </li>
        ))}
      </ol>
      </div>

    )
  }
}
