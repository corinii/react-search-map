import React, {Component} from 'react'
import Map from './Map'
import SideMenu from './SideMenu'
import './Homepage.css'
import axios from 'axios'

export default class Homepage extends Component {
  state = {
    venues: []
  }

  componentDidMount() {
    this.getVenues()
  }
  renderMap = () => {
    loadScript("https://maps.googleapis.com/maps/api/js?key=&callback=initMap")
    window.initMap = this.initMap
  }
  getVenues = () => {
    const endPoint = "https://api.foursquare.com/v2/venues/explore?"
    const parameters = {
      client_id: "UZY32QRUHLAWXGNAXWOOYCNEIUKQX4NMT5SZXFYGT0H52FUM",
      client_secret: "RPTAAHS54VLWG1VU1XQL31VCBS1XVJMJH0US01JDQ4FZXECM",
      query: "coffee",
      near: "Zurich",
      limit: 15,
      venuePhotos: 1,
      v: "20180805"
    }

    axios.get(endPoint + new URLSearchParams(parameters)).then(response => {
      this.setState({
        venues: response.data.response.groups[0].items
      }, this.renderMap())
    }).catch(error => {
      console.log("Ouups! Something is wrong!" + error)
    })
  }
  initMap = () => {
    const map = new window.google.maps.Map(document.getElementById('map'), {
      center: {
        lat: 47.3769,
        lng: 8.5417
      },
      zoom: 13
    });

    const infowindow = new window.google.maps.InfoWindow()

    // display markers
    this.state.venues.map(myVenue => {
      let contentString = `${myVenue.venue.name}`

    //create markers
      const marker = new window.google.maps.Marker({
        map: map,
        animation: window.google.maps.Animation.DROP,
        position: {
          lat: myVenue.venue.location.lat,
          lng: myVenue.venue.location.lng
        },
        title: myVenue.venue.name
      });

    marker.addListener('click', function toggleBounce () {
       if (marker.getAnimation() !== null) {
         marker.setAnimation(null);
       } else {
         marker.setAnimation(window.google.maps.Animation.BOUNCE);
       }
     });
        marker.addListener('click', function() {
        infowindow.setContent(contentString)
        infowindow.open(map, marker)
        marker.setAnimation(window.google.maps.Animation.BOUNCE)
      })
}
    )
  }

//Sidebar
  render() {
    return (
        <div className="homepage" id ="homepage">
          <SideMenu venues={this.state.venues} />
        <Map />
        </div>
     )
  }
}

  //<SideMenu {...this.state}/>

  function loadScript(url) {
  	var index = window.document.getElementsByTagName("script")[0]
  	var script = window.document.createElement("script")
  	script.src = url
  	script.async = true
  	script.defer = true
  	index.parentNode.insertBefore(script,index)
  }
